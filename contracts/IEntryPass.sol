// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

/**
 * @dev Interface of the Entrypas token implementation.
 */

interface IEntryPass { 

    event newNFTCreated(uint256 tokenId);

    event newAirDropToken(uint256 tokenId);

    event setNFTValue(uint256 nftPrice);

    function setNFTPrice(uint256 _nftValue) external;

    function baseURI() external view returns (string memory);

    function pause() external;

    function unpause() external; 

    function mintNFTToken() external payable;   

    function airDrop(address recipient) external;
}