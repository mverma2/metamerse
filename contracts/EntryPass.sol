// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/Counters.sol";

import "./IEntryPass.sol";

contract EntryPass is ERC721, Pausable, Ownable, IEntryPass {
    
    using Counters for Counters.Counter;
    
    uint256 public nftPrice;

    mapping(uint256 => bool) public airDroppedTokens;        

    string private baseUri;

    Counters.Counter private _tokenIdCounter;
    
    constructor(
        string memory _name, 
        string memory _symbol, 
        uint256 _price,
        string memory _baseUri        
    ) 
    ERC721(_name, _symbol)
    {
        baseUri = _baseUri;
        nftPrice = _price;
    }

    function setNFTPrice(uint256 _nftValue) external override onlyOwner 
    {
        nftPrice = _nftValue;
        emit setNFTValue(nftPrice);
    }

    function _baseURI() internal view override returns (string memory) 
    {
        return baseUri;
    }

    function baseURI() external view override returns (string memory) 
    {
        return _baseURI();
    }

    function pause() external override onlyOwner 
    {
        _pause();
    }

    function unpause() external override onlyOwner 
    {
        _unpause();
    }

    function mintNFTToken() external override payable
    {
        require(msg.value >= nftPrice, "Not enough ETH to Mint the NFT; Please check price!");
        uint256 tokenId = _tokenIdCounter.current();
        _tokenIdCounter.increment();
        _safeMint(msg.sender, tokenId);
        emit newNFTCreated(tokenId);
    }    

    function airDrop(address recipient) external override onlyOwner
    {
        uint256 tokenId = _tokenIdCounter.current();
        _tokenIdCounter.increment();
        _safeMint(recipient, tokenId);
        airDroppedTokens[tokenId] = true;
        emit newAirDropToken(tokenId);
    }

    function _beforeTokenTransfer(address from, address to, uint256 tokenId) internal whenNotPaused override
    {
        super._beforeTokenTransfer(from, to, tokenId);
    }
        
}