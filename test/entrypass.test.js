const {
    BN, // Big Number support
    constants, // Common constants, like the zero address and largest integers
    expectEvent, // Assertions for emitted events
    expectRevert, // Assertions for transactions that should fail
    time,
  } = require("@openzeppelin/test-helpers");

const { fromWei, toWei } = require("web3-utils");

const EntryPass = artifacts.require("EntryPass");


contract("EntryPass", (accounts) => {
    let entrypassInstance = null;
    const [owner, user, bob, steve, blackListUser, george] = accounts;    

    const eventname = {
        Transfer: "Transfer",
        newNFTCreated: "newNFTCreated",
        newAirDropToken: "newAirDropToken",
        Paused: "Paused",
        Unpaused: "Unpaused",
        setNFTValue: "setNFTValue"

    };

    let total = 0;    
    const name = "Entry Pass Contract";
    const symbol = "EPC";
    let mintPrice = toWei("0.1", "ether");
    const baseUri = "https://www.google.com/";

    before("Deploy new Entry Pass contract", async () => {
        await EntryPass.new(name, symbol, mintPrice, baseUri, { from: owner }).then(
            (instance) => (entrypassInstance = instance)
        );
    });

    describe("New Entry Pass Contract", async() => {
        it('has a name', async function () {
            expect(await entrypassInstance.name()).to.equal(name);
        });

        it('has a symbol', async function () {
            expect(await entrypassInstance.symbol()).to.equal(symbol);
        });

        it('has a baseUri', async function () {
            expect(await entrypassInstance.baseURI()).to.equal(baseUri);
        });

    });

    describe("Set NFT Price", async() => {
        describe("When owner set the NFT Price", async() => {
            it('should set the price', async function() {
                const unpauseContract = await entrypassInstance.setNFTPrice(mintPrice,
                    {
                        from: owner,
                    }
                );
                await expectEvent(unpauseContract, eventname.setNFTValue, {
                    nftPrice : mintPrice
                });
            });
        });
    });    

    describe("Mint Nft", async() => {
        describe("When user mint a NFT", async() => {
            it('should mint token', async function() {
                const mintNftReceipt = await entrypassInstance.mintNFTToken(
                    {
                        from: user,
                        value: mintPrice.toString()              
                    }
                );
                await expectEvent(mintNftReceipt, eventname.Transfer, {
                    from : constants.ZERO_ADDRESS,
                    to : user,
                    tokenId : total.toString()
                });
    
                await expectEvent(mintNftReceipt, eventname.newNFTCreated, {
                    tokenId : total.toString()
                });    
                total++;
            });            
        });

        describe("When price is low", async() => {
            it('should not mint this token', async function() {
                await expectRevert(
                    entrypassInstance.mintNFTToken(
                        {
                            from: user,
                            value: 0              
                        }
                    ),
                    "Not enough ETH to Mint the NFT"
                );
            });            
        })
    });

    describe("Airdrop", async() => {
        describe("When owner tries to airdrop", async() => {
            it('should airdrop token', async function() {
                const airdropReceipt = await entrypassInstance.airDrop(user, {
                    from: owner,
                });
                await expectEvent(airdropReceipt, eventname.Transfer, {
                    from : constants.ZERO_ADDRESS,
                    to : user,
                    tokenId : total.toString()
                });

                await expectEvent(airdropReceipt, eventname.newAirDropToken, {
                    tokenId : total.toString()
                });
                total++;
            });
        })

        describe("When other user tries to airdrop", async() => {
            it('should not airdrop token', async function() {
                
                await expectRevert(
                    entrypassInstance.airDrop(user, {
                        from: user,
                    }),
                    "Ownable: caller is not the owner"
                );
            });
        })
    });

    describe("Pause Contract", async() => {
        describe("When owner pause a contract", async() => {
            it('should pause the contract', async function() {
                const pauseContract = await entrypassInstance.pause(
                    {
                        from: owner,
                    }
                );
                await expectEvent(pauseContract, eventname.Paused, {
                    account : owner
                });
            });
        });

        describe("When other user pause a contract", async() => {
            it('should not pause the contract', async function() {
                await expectRevert(
                    entrypassInstance.pause(
                        {
                            from: user,
                        }
                    ),
                    "Ownable: caller is not the owner"
                );
            });
        });
    });


    describe("Unpause Contract", async() => {
        describe("When owner unpause a contract", async() => {
            it('should unpause the contract', async function() {
                const unpauseContract = await entrypassInstance.unpause(
                    {
                        from: owner,
                    }
                );
                await expectEvent(unpauseContract, eventname.Unpaused, {
                    account : owner
                });
            });
        });

        describe("When other user unpause a contract", async() => {
            it('should not unpause the contract', async function() {
                await expectRevert(
                    entrypassInstance.unpause(
                        {
                            from: user,
                        }
                    ),
                    "Ownable: caller is not the owner"
                );
            });
        });
    });

    
});