const EntryPass = artifacts.require("EntryPass");

module.exports = function (deployer) {
  deployer.deploy(EntryPass,"Entry Pass", "EPT", 1, "https://www.google.com/");
};
